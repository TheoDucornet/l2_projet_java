package ulco.cardGame.Client;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.interfaces.Game;
import ulco.cardGame.common.interfaces.Player;
import ulco.cardGame.server.SocketServer;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Client extends Application {

    private Scene scene;
    private AnchorPane root;

    public void start(Stage stage){
        this.root= new AnchorPane();
        this.scene= new Scene(this.root,1200,1200);
        stage.setScene(scene);
        stage.setResizable(false);
    }


    public static void main(String[] args){

        // Current use full variables
        try {

            // Create a connection to the server socket on the server application
            InetAddress host = InetAddress.getLocalHost();
            Socket socket;

            Object answer;
            String username;

            Scanner scanner = new Scanner(System.in);

            System.out.println("Please select your username");
            username = scanner.nextLine();

            socket = new Socket(host.getHostName(), SocketServer.PORT);

            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            oos.writeObject(username);

            Player player=null;

            do {
                // Read and display the response message sent by server application
                ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                answer = ois.readObject();

                // depending of object type, we can manage its data
                if (answer instanceof String)
                    System.out.println(answer.toString());
                if (answer instanceof Game) {
                    ((Game) answer).displayState();
                    player=((Game) answer).getCurrentPlayer(username);
                }
                if(answer.equals("["+player.getName()+"] you have to play...(press enter)")){
                    //scanner.nextLine();
                    player.play(socket);
                }
                if(answer.equals("Check your cards")){
                    player.displayHand();
                }
                if(answer.equals("["+player.getName()+"] Choose a coin (Red,Blue,Black)")){
                    player.play(socket);
                }
                if(answer.equals("Game started: Battle")) {
                    System.out.println("passe");
                    //launch(args);
                }

            } while (!answer.equals("END"));

            // close the socket instance connection
            socket.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
