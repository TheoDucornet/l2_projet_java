package ulco.cardGame.common.interfaces;

import ulco.cardGame.common.games.components.Component;

import java.io.Serializable;
import java.util.List;

public interface Board extends Serializable {

    void clear();

    void addComponent(Component component);

    List<Component> getComponents();

    List<? extends Component> getSpecificComponents(Class c);

    void displayState();
}
