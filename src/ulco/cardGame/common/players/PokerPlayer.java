package ulco.cardGame.common.players;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Coin;
import ulco.cardGame.common.games.components.Component;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class PokerPlayer extends BoardPlayer{
    private List<Card> cards;
    private List<Coin> coins;

    public PokerPlayer(String name){
        super(name);
        cards=new ArrayList<>();
        coins= new ArrayList<>();
    }

    public Integer getScore(){return score;}

    public void play(Socket socket){
        Scanner scanner= new Scanner(System.in);
        Coin playedCoin=null;
        while(playedCoin == null){
            System.out.println(getName()+", please select a valid coin to play (coin color)");
            String color=scanner.nextLine();
            for(Coin coin: coins){
                if(coin.getName().equals(color)){
                    playedCoin=coin;
                    break;
                }
            }
        }
        coins.remove(playedCoin);
        try {
            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            oos.writeObject(playedCoin);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addComponent(Component component) {
        if(component instanceof Card)
            cards.add((Card)component);
        else if(component instanceof Coin) {
            //Ici le score est représenté par les jetons
            score=score+component.getValue();
            coins.add((Coin) component);
        }
    }

    public void removeComponent(Component component){
        if(component instanceof Card)
            cards.remove(component);
        else if(component instanceof Coin) {
            score=score-component.getValue();
            //Les pièces sont envoyées par le joueur donc il faut changer cette méthode
            Coin playedCoin= null;
            for(Coin coin: coins){
                if(coin.getId().equals(component.getId())){
                    playedCoin=coin;
                    break;
                }
            }
            coins.remove(playedCoin);
        }
    }

    public List<Component> getComponents(){
        List<Component> components= new ArrayList<>();
        components.addAll(cards);
        components.addAll(coins);
        return components;
    }

    public List<? extends Component> getSpecificComponents(Class classType){
        if(classType == Card.class)
            return cards;
        else if(classType==Coin.class)
            return coins;
        return null;
    }

    public void shuffleHand(){
        Collections.shuffle(cards);
    }

    public void clearHand(){
        cards.clear();
    }

    public void displayHand(){
        System.out.println("=>"+getName()+"\n-Cards :");
        for(Card card: cards){
            System.out.println(card);
        }
        System.out.println("-Coins:");
        //On parcours une première fois la liste pour savoir combien d'exemplaires de jetons on possède
        int numberRed=0,numberBlue=0,numberBlack=0;
        for(Coin coin: coins){
            if(coin.getName().equals("Red"))
                numberRed++;
            if(coin.getName().equals("Blue"))
                numberBlue++;
            if(coin.getName().equals("Black"))
                numberBlack++;
        }
        //On parcours la liste pour savoir quelle couleur de pièce afficher, et comptabiliser le total de points
        int total=0;
        boolean red=true,blue=true,black=true;
        for(Coin coin : coins){
            total=total+coin.getValue();
            if(coin.getName().equals("Red") && red) {
                System.out.println("Coin " + coin.getName() + " x " + numberRed);
                red = false;
            }
            if(coin.getName().equals("Blue") && blue) {
                System.out.println("Coin " + coin.getName() + " x " + numberBlue);
                blue = false;
            }
            if(coin.getName().equals("Black") && black) {
                System.out.println("Coin " + coin.getName() + " x " +numberBlack);
                black = false;
            }
        }
        System.out.println("-Total :"+total+"\n");
    }

    public String toString(){
        String str="Player: "+name+" Score: "+score+" Etat: ";
        if(playing)
            str+="playing";
        else
            str+=" not playing";
        return str;
    }
}
