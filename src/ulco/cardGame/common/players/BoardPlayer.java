package ulco.cardGame.common.players;

import ulco.cardGame.common.interfaces.Player;

public abstract class BoardPlayer implements Player {
    String name;
    boolean playing;
    Integer score;

    public BoardPlayer(String name){
        this.name=name;
        playing=true;
        score=0;
    }

    public void canPlay(boolean playing){this.playing=playing;}

    public String getName(){return name;}

    public boolean isPlaying() {return playing;}

    public String toString(){
        String str="Player: "+name+" Score: "+score+"Etat: ";
        if(playing)
            str+="playing";
        else
            str+=" not playing";
        return str;
    }
}
