package ulco.cardGame.common.players;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Component;

import javax.imageio.IIOException;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class CardPlayer extends BoardPlayer {
    List<Card> cards;

    public CardPlayer(String name){
        super(name);
        cards=new ArrayList<Card>();
    }

    public Integer getScore(){return score;}

    public void addComponent(Component component){
        if(component instanceof Card){
        cards.add((Card)component);
        score=score+1;
        }
    }

    public void removeComponent(Component component){
        for(int i=0;i<cards.size();i++){
            if(component.getId().equals(cards.get(i).getId())){
                cards.remove(cards.get(i));
                score=score-1;
            }
        }
    }

    public void play(Socket socket) {
        Card playedCard=cards.get(0);
        removeComponent(playedCard);
        try {
            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            oos.writeObject(playedCard);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Component> getComponents(){
        List <Component> cardsList= new ArrayList<Component>(cards);
        return cardsList;
    }

    public void shuffleHand(){
        Collections.shuffle(cards);
    }

    public void clearHand(){
        cards.clear();
    }

    public String toString(){
        String str="Player: "+name+" Score: "+score+" Etat: ";
        if(playing)
            str+="playing";
        else
            str+=" not playing";
        return str;
    }

    public List<? extends Component> getSpecificComponents(Class classType){
        if(classType==Card.class)
            return getComponents();
        return null;
    }

    public void displayHand() {
        System.out.println(getName()+" has "+getScore()+" cards.");
    }
}
