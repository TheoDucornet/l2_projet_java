package ulco.cardGame.common.games;

import ulco.cardGame.common.games.boards.PokerBoard;
import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Coin;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Player;

import java.io.*;
import java.net.Socket;
import java.util.*;

public class PokerGame extends BoardGames{
    private List<Card> cards;
    private List<Coin> coins;
    private int maxRounds=3;
    private int numberOfRounds;

    public PokerGame(String name, Integer maxPlayers,String filename){
        super(name,maxPlayers,filename);
        board=new PokerBoard();
    }

    //J'override displayState pour afficher les cartes sur le board
    public void displayState(){
        System.out.println("---------------------------------------------");
        System.out.println("--------------- Game  State  ----------------");
        System.out.println("---------------------------------------------");
        System.out.println("Players :");
        for(int i=0;i<players.size();i++){
            System.out.println(i+"-"+players.get(i));
        }
        getBoard().displayState();
        System.out.println("---------------------------------------------\n");
    }

    public void initialize(String filename){
        try {
            File componentsFile = new File(filename);
            Scanner myReader = new Scanner(componentsFile);
            //On initialise la liste des composants:
            cards=new ArrayList<>();
            coins=new ArrayList<>();
            while (myReader.hasNextLine()) {
                String data=myReader.nextLine();
                String Text[] = data.split(";");
                //On sépare cartes et jetons
                if(Text[0].equals(Card.class.getSimpleName())){
                    Card card = new Card(Text[1], Integer.parseInt(Text[2]),true);
                    this.cards.add(card);
                }
                else if(Text[0].equals((Coin.class.getSimpleName()))){
                    Coin coin= new Coin(Text[1],Integer.parseInt(Text[2]));
                    this.coins.add(coin);
                }
            }
            myReader.close();
        } catch (FileNotFoundException e){
            System.out.println("An error occured.");
            e.printStackTrace();
        }
    }

    public Player run(Map<Player, Socket> playerSockets){
        Player winner=null;
        try {
            if (!isStarted()) {
                System.out.println("Not enough players !");
                for (Socket playerSocket : playerSockets.values()) {

                    ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                    playerOos.writeObject("Not enough players ! ");
                }
                return null;
            }
            //Distribution des jetons
            for (Player player : getPlayers()) {
                List<Coin> copiedCoins = new ArrayList<>(coins);
                for (Coin coin : copiedCoins) {
                    player.addComponent(coin);
                }
            }
            displayState();
            //On commence les rounds
            while (!end()) {
                //Mélange du paquet
                Collections.shuffle(cards);
                for (Socket playerSocket : playerSockets.values()) {
                    ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                    playerOos.writeObject("\n==============Round Number " + (numberOfRounds + 1) + "==============");
                }
                System.out.println("\n==============Round Number " + (numberOfRounds + 1) + "==============");
                //On distribue 3 cartes à tous les joueurs
                for (int i = 0; i < 3; i++) {
                    for (Player player : getPlayers()) {
                        if (!player.isPlaying())
                            continue;
                        player.addComponent(cards.get(0));
                        cards.remove(cards.get(0));
                    }
                }
                //On actualise le jeu des joueurs avec les jetons et leurs cartes
                for (Socket playerSocket : playerSockets.values()) {
                    ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                    playerOos.writeObject(this);
                }
                //On affiche les cartes des joueurs
                for (Socket playerSocket : playerSockets.values()) {
                    ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                    playerOos.writeObject("Check your cards");
                }
                //On permet à chaque joueurs de miser
                for(Player player: players){
                    for(Socket playerSocket: playerSockets.values()){
                        ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                        playerOos.writeObject("["+player.getName()+"] Choose a coin (Red,Blue,Black)");
                    }
                    ObjectInputStream ois = new ObjectInputStream(playerSockets.get(player).getInputStream());
                    Coin playedCoin = (Coin) ois.readObject();
                    getBoard().addComponent(playedCoin);
                    player.removeComponent(playedCoin);
                }
                //On ajoute 3 cartes sur le plateau de jeu
                for (int i = 0; i < 3; i++) {
                    cards.get(0).setHidden(false);
                    getBoard().addComponent(cards.get(0));
                    cards.remove(0);
                }
                System.out.println("\n 3 cards added to the board !!");
                for (Socket playerSocket : playerSockets.values()) {
                    ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                    playerOos.writeObject(this);
                    ObjectOutputStream playerOos2 = new ObjectOutputStream(playerSocket.getOutputStream());
                    playerOos2.writeObject(this.getBoard());
                }
                //On permet aux joueurs de miser à nouveau
                for(Player player: players){
                    for(Socket playerSocket: playerSockets.values()){
                        ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                        playerOos.writeObject("["+player.getName()+"] Choose a coin (Red,Blue,Black)");
                    }
                    ObjectInputStream ois = new ObjectInputStream(playerSockets.get(player).getInputStream());
                    Coin playedCoin = (Coin) ois.readObject();
                    getBoard().addComponent(playedCoin);
                    player.removeComponent(playedCoin);
                }
                //On cherche le nombre maximum de cartes identiques
                int maxIdenticalCard = 0;
                for (Player player : getPlayers()) {
                    //On combine les cartes de la main du joueur et celles du plateau
                    List<Component> allCards = new ArrayList(player.getSpecificComponents(Card.class));
                    allCards.addAll(board.getSpecificComponents(Card.class));
                    //On recupères toutes les valeurs pour pouvoir utiliser frequency
                    List<Integer> allValue = new ArrayList<>();
                    for (Component component : allCards) {
                        allValue.add(component.getValue());
                        //To test:
                        //System.out.println(component.getValue());
                    }
                    //On cherche le nombre maximum de cartes identiques
                    for (Integer number : allValue) {
                        if (Collections.frequency(allValue, number) > maxIdenticalCard)
                            maxIdenticalCard = Collections.frequency(allValue, number);
                    }
                    //To test:
                    //System.out.println("Maximum identical cards : "+maxIdenticalCard+" after "+player.getName());
                }
                //On cherche les gagnants cad ceux qui ont le nombre maximum de cartes identiques
                Map<Player, Integer> winners = new HashMap<>();
                for (Player player : getPlayers()) {
                    //On combine les cartes de la main du joueur et celles du plateau
                    List<Component> allCards = new ArrayList(player.getSpecificComponents(Card.class));
                    allCards.addAll(board.getSpecificComponents(Card.class));
                    //On recupères toutes les valeurs pour pouvoir utiliser frequency
                    List<Integer> allValue = new ArrayList<>();
                    for (Component component : allCards) {
                        Card card = (Card) component;
                        card.setHidden(false);
                        allValue.add(card.getValue());
                    }
                    //On cherche ceux qui ont le bon nombre de cartes identiques
                    for (Integer number : allValue) {
                        if (Collections.frequency(allValue, number) == maxIdenticalCard) {
                            winners.put(player, number);
                            break;
                        }
                    }
                }
                //Gestion d'égalités
                //On cherche le numéro de carte le plus haut
                int highestCardValue = 0;
                for (Map.Entry<Player, Integer> entry : winners.entrySet()) {
                    if (entry.getValue() > highestCardValue)
                        highestCardValue = entry.getValue();
                }
                //On cherche ceux avec le numéro de cartes le plus élevé
                List<Player> winner2 = new ArrayList<>();
                for (Map.Entry<Player, Integer> entry : winners.entrySet()) {
                    if (entry.getValue() == highestCardValue) {
                        winner2.add(entry.getKey());
                        for (Socket playerSocket : playerSockets.values()) {
                            ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                            playerOos.writeObject("Winner of the round is " + entry.getKey().getName() + " " +
                                    "with " + maxIdenticalCard + " identical cards (Value: " + highestCardValue + ")");
                        }
                        System.out.println("Winner of the round is " + entry.getKey().getName() + " " +
                                "with " + maxIdenticalCard + " identical cards (Value: " + highestCardValue + ")");
                    }
                }
                //On distribue les jetons aux gagnant, aléatoirement pour gérer les égalités extrêmes
                for (Component component : getBoard().getSpecificComponents(Coin.class)) {
                    Collections.shuffle(winner2);
                    for (Socket playerSocket : playerSockets.values()) {
                        ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                        playerOos.writeObject(winner2.get(0).getName() + " get a " + component.getName() + "Coin");
                    }
                    System.out.println(winner2.get(0).getName() + " get a " + component.getName() + "Coin");
                    winner2.get(0).addComponent(component);
                }

                //On nettoie le plateau:
                for (Component component : getBoard().getSpecificComponents(Card.class)) {
                    cards.add((Card) component);
                }
                getBoard().getSpecificComponents(Card.class).clear();
                getBoard().getSpecificComponents(Coin.class).clear();

                //On récupère les cartes des joueurs
                for (Player player : players) {
                    for (Component component : player.getSpecificComponents(Card.class)) {
                        cards.add((Card) component);
                    }
                    player.getSpecificComponents(Card.class).clear();
                }
                for (Card card : cards) {
                    card.setHidden(true);
                }
                numberOfRounds++;
            }
            //Affichage des jetons tous les joueurs
            int highestscore = 0;
            for (Player player : getPlayers()) {
                player.displayHand();
                if (player.getScore() > highestscore)
                    highestscore = player.getScore();
            }
            for (Player player : getPlayers()) {
                if (player.getScore() == highestscore)
                    winner = player;
            }

        }catch(IOException | ClassNotFoundException e){
            e.printStackTrace();
        }
        return winner;
    }

    public boolean end(){
        if(numberOfRounds==maxRounds)
            return true;
        return false;
    }
}
