package ulco.cardGame.common.games;

import ulco.cardGame.common.games.boards.CardBoard;
import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.interfaces.Player;

import java.io.*;
import java.net.Socket;
import java.util.*;

public class CardGame extends BoardGames{
    protected List<Card> cards;
    protected Integer numberOfRounds;

    public CardGame(String name,Integer maxPlayers,String filename){
        super(name,maxPlayers,filename);
        board=new CardBoard();
        numberOfRounds=0;
    }
    public void initialize(String filename){
        try {
            File cardFile = new File(filename);
            Scanner myReader = new Scanner(cardFile);
            //On initialise la liste des cartes:
            cards=new ArrayList<Card>();
            while (myReader.hasNextLine()) {
                //Création des cartes
                String data=myReader.nextLine();
                String cardText[] = data.split(";");
                Card card = new Card(cardText[0], Integer.parseInt(cardText[1]),true);
                this.cards.add(card);
            }
            myReader.close();
        } catch (FileNotFoundException e){
            System.out.println("An error occured.");
            e.printStackTrace();
        }
    }

    public Player run(Map<Player, Socket> playerSockets){
        try {
            if (!isStarted()) {
                System.out.println("Not enough players !");
                for (Socket playerSocket : playerSockets.values()) {

                    ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                    playerOos.writeObject("Not enough players ! ");
                }
                return null;
            }
            //Mélange des cartes
            Collections.shuffle(cards);
            //Distribution des cartes
            int numjoueur = 0;
            while (!cards.isEmpty()) {
                Player currentPlayer = players.get(numjoueur);
                Card currentCard = cards.get(0);
                currentPlayer.addComponent(currentCard);
                cards.remove(currentCard);
                numjoueur++;
                if (numjoueur >= players.size())
                    numjoueur = 0;
            }
            System.out.println("Game started: " + name);
            //On transmet une première fois le jeu aux joueurs
            for (Socket playerSocket : playerSockets.values()) {
                ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                playerOos.writeObject(this);
            }
            //On leur demande de lancer l'interface graphique
            for (Socket playerSocket : playerSockets.values()) {
                ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                playerOos.writeObject("Game started: " + name);
            }
            displayState();
            //Rounds:
            while (!end()) {
                numberOfRounds++;
                board.clear();
                System.out.println("----Round " + numberOfRounds + "----");
                for (Socket playerSocket : playerSockets.values()) {

                    ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                    playerOos.writeObject("----Round " + numberOfRounds + "----");
                }
                //Tous les 10 rounds, chaque joueurs mélange sa main:
                if (numberOfRounds % 10 == 0) {
                    System.out.println("Every players shuffle their cards !");
                    for (Socket playerSocket : playerSockets.values()) {

                        ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                        playerOos.writeObject("Every players shuffle their cards !");
                    }
                    for (Player player : players) {
                        if (player.isPlaying())
                            player.shuffleHand();
                    }
                }
                //Chaque joueur joue sa 1ere carte, on conserve la plus haute valeur jouée:
                Map<Player, Card> playedCard = new HashMap<>();
                int highestValue = 0;
                for (Player player : players) {
                    if (player.isPlaying()) {
                        //On préviens les autres joueurs de qui joue
                        for (Socket playerSocket : playerSockets.values()) {
                            if(player != playerSockets.get(playerSocket)) {
                                ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                                playerOos.writeObject(player.getName() + " is playing");
                            }
                        }
                        //Demande de la carte
                        ObjectOutputStream oos = new ObjectOutputStream(playerSockets.get(player).getOutputStream());
                        oos.writeObject(this);
                        ObjectOutputStream obs = new ObjectOutputStream(playerSockets.get(player).getOutputStream());
                        obs.writeObject("["+player.getName()+"] you have to play...(press enter)");
                        ObjectInputStream ois = new ObjectInputStream(playerSockets.get(player).getInputStream());
                        Card card = (Card) ois.readObject();
                        //On met la carte sur le Board
                        player.removeComponent(card);
                        card.setHidden(false);
                        playedCard.put(player, card);
                        board.addComponent(card);
                        System.out.println(player.getName() + " has played " + card.getName());
                        for (Socket playerSocket : playerSockets.values()) {
                            ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                            playerOos.writeObject(player.getName() + " has played " + card.getName());
                        }
                        if (card.getValue() > highestValue)
                            highestValue = card.getValue();
                    }
                }
                //On affiche le plateau
                board.displayState();
                for (Socket playerSocket : playerSockets.values()) {

                    ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                    playerOos.writeObject(board);
                }
                //On determine la liste de gagnants:
                List<Player> winners = new ArrayList<>();
                for (Map.Entry<Player, Card> entry : playedCard.entrySet()) {
                    Card card = entry.getValue();
                    if (card.getValue() == highestValue)
                        winners.add(entry.getKey());
                }
                //On shuffle la liste et on prend le 1er de la liste pour choisir aléatoirement en cas d'égalité:
                Collections.shuffle(winners);
                Player winner = winners.get(0);
                System.out.println("=>Round Winner is " + winner.getName() + " (" + winner.getScore() + ")");
                for (Socket playerSocket : playerSockets.values()) {

                    ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                    playerOos.writeObject("=>Round Winner is " + winner.getName() + " (" + winner.getScore() + ")");
                }
                //Le gagnant récupère toutes les cartes:
                for (Map.Entry<Player, Card> entry : playedCard.entrySet()) {
                    Card card = entry.getValue();
                    card.setHidden(true);
                    winner.addComponent(card);
                }
                //On change l'état de ceux qui ne peuvent plus jouer:
                for (Player player : players) {
                    if (player.getScore() == 0 && player.isPlaying()) {
                        player.canPlay(false);
                        displayState();
                        System.out.println(player.getName() + " lost, he is out of Cards !");
                        for (Socket playerSocket : playerSockets.values()) {

                            ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                            playerOos.writeObject(player.getName() + " lost, he is out of Cards !");
                        }
                    }
                }
            }
            //A la fin des rounds, on renvoie le gagnant, cad le seul qui peut encore jouer:
            for (Player player : players) {
                if (player.isPlaying()) {
                    player.canPlay(false);
                    return player;
                }
            }
        }catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        //En cas d'erreur
        return null;
    }

    public boolean end(){
        //Recherche d'un gagnant
        for(Player player: players){
            if(player.getScore()==52) {
                endGame= true;
            }
        }
        //Partie non terminée
        return endGame;
    }

    public String toString(){
        String str="Game :"+this.name;
        return str;
    }
}
