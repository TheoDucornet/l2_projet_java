package ulco.cardGame.common.games.components;

public class Card extends Component{
    private boolean hidden;

    public Card(String name, Integer value, boolean hidden){
        super(name,value);
        this.hidden=hidden;
    }

    public boolean isHidden(){return this.hidden;}

    public void setHidden(boolean hidden){this.hidden=hidden;}

    @Override
    public String toString() {
        return "Card{" +
                "name='" + name + '\'' +
                ", value=" + value + '\'' +
                ", hidden="+ hidden +
                '}';
    }
}
