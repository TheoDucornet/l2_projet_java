package ulco.cardGame.common.games;

import ulco.cardGame.common.games.boards.CardBoard;
import ulco.cardGame.common.interfaces.Board;
import ulco.cardGame.common.interfaces.Game;
import ulco.cardGame.common.interfaces.Player;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class BoardGames implements Game {
    protected String name;
    protected Integer maxPlayers;
    protected List<Player> players;
    protected boolean endGame;
    protected boolean started;
    protected Board board;

    public BoardGames(String name, Integer maxPlayers, String filename){
        this.name=name;
        this.maxPlayers=maxPlayers;
        this.players=new ArrayList<>();
        endGame=false;
        started=false;
        initialize(filename);
    }

    public boolean addPlayer(Socket socket,Player player){
        try {
            //Si le pseudo n'est pas déjà utilisé dans la partie
            for (Player player1: players) {
                if (player1.getName().equals(player.getName())) {
                    ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
                    oos.writeObject("Le pseudonyme est déjà utilisé !");
                    return false;
                }
            }
            //Si le nbre de joueurs max n'est pas atteint
            if (players.size() >= maxPlayers) {
                ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
                oos.writeObject("Le nombre de joueurs max est déjà atteint !");
                return false;
            }
        }catch (IOException  e) {
                e.printStackTrace();
            }
        //On ajoute le joueur
        players.add(player);
        //Si on arrive au nbre max, on la partie peut être lancée
        if (players.size() == maxPlayers) {
            started = true;
        }
        return true;
    }

    public void removePlayer(Player player){
        players.remove(player);
    }

    public void removePlayers(){
        players.clear();
    }

    public void displayState(){
        System.out.println("---------------------------------------------");
        System.out.println("--------------- Game  State  ----------------");
        System.out.println("---------------------------------------------");
        System.out.println("Players :");
        for(int i=0;i<players.size();i++){
            System.out.println(i+"-"+players.get(i));
        }
        System.out.println("---------------------------------------------\n");
    }

    public boolean isStarted() {
        return started;
    }

    public Integer maxNumberOfPlayers(){
        return maxPlayers;
    }

    public List<Player> getPlayers(){
        return players;
    }

    public Board getBoard() {return board;}

    public Player getCurrentPlayer(String name) {
        for(Player player:players)
            if(player.getName().equals(name))
                return player;
        return null;
    }
}
