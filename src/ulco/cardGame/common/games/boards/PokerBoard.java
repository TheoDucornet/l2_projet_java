package ulco.cardGame.common.games.boards;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Coin;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Board;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PokerBoard implements Board {

    private List<Card> cards;
    private List<Coin> coins;

    public PokerBoard(){
        cards=new ArrayList<>();
        coins= new ArrayList<>();
    }

    public void clear(){
        cards.clear();
        coins.clear();
    }

    public void addComponent(Component component) {
        if(component instanceof Card){
            cards.add((Card)component);
        }
        else if(component instanceof Coin){
            coins.add((Coin) component);
        }
    }

    public List<Component> getComponents(){
        List<Component> components= new ArrayList<>();
        for(Card card: cards){
            components.add(card);
        }
        for(Coin coin: coins){
            components.add(coin);
        }
        return components;
    }

    public List<? extends Component> getSpecificComponents(Class c) {
        if(c==Card.class){
            return cards;
        }
        else if(c==Coin.class){
            return coins;
        }
        return null;
    }

    public void displayState(){
        System.out.println("==Board  State==");
        System.out.print("Cards: ");
        for(Card card: cards){
            if(!card.isHidden())
                System.out.print(card.getName()+"("+card.getValue()+")\t");
        }

        //On parcours une première fois la liste pour savoir combien d'exemplaires de jetons on possède
        int numberRed=0,numberBlue=0,numberBlack=0;
        for(Coin coin: coins){
            if(coin.getName().equals("Red"))
                numberRed++;
            if(coin.getName().equals("Blue"))
                numberBlue++;
            if(coin.getName().equals("Black"))
                numberBlack++;
        }
        if(numberBlack!=0||numberBlue!=0||numberRed!=0)
            System.out.println("\nCoins:");
        //On parcours la liste pour savoir quelle couleur de pièce afficher, et comptabiliser le total de points
        int total=0;
        boolean red=true,blue=true,black=true;
        for(Coin coin : coins){
            total=total+coin.getValue();
            if(coin.getName().equals("Red") && red) {
                System.out.println("Coin " + coin.getName() + " x " + numberRed);
                red = false;
            }
            if(coin.getName().equals("Blue") && blue) {
                System.out.println("Coin " + coin.getName() + " x " + numberBlue);
                blue = false;
            }
            if(coin.getName().equals("Black") && black) {
                System.out.println("Coin " + coin.getName() + " x " +numberBlack);
                black = false;
            }
        }
        System.out.println();
    }
}
