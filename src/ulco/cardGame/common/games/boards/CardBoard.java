package ulco.cardGame.common.games.boards;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Board;

import java.util.ArrayList;
import java.util.List;

public class CardBoard implements Board {

    private List<Card> cards;

    public CardBoard(){
        cards=new ArrayList<>();
    }

    public void clear(){
        cards.clear();
    }

    public void addComponent(Component component){
        cards.add((Card)component);
    }

    public List<Component> getComponents(){
        List<Component> components= new ArrayList<>();
        for(Card card: cards){
            components.add(card);
        }
        return components;
    }

    public List<? extends Component> getSpecificComponents(Class classType) {
        List<Card> components = new ArrayList<>();
        for (Card card : cards) {
            if (card.getClass() == classType)
                components.add(card);
        }
        return components;
    }

    public void displayState(){
        System.out.println("==Board  State==");
        for(Card card: cards){
            if(!card.isHidden())
                System.out.print(card.getName()+"("+card.getValue()+")\t");
        }
        System.out.println();
    }
}
