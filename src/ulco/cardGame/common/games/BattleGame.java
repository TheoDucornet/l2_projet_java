package ulco.cardGame.common.games;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.interfaces.Player;

import java.net.Socket;
import java.util.*;

public class BattleGame extends CardGame {

    public BattleGame(String name,Integer maxPlayers,String filename){
        super(name,maxPlayers,filename);
    }

    public Player run(Map<Player, Socket> PlayerSocket){

        if(!isStarted()){
            System.out.println("Not enough players !");
            return null;
        }
        //Mélange des cartes
        Collections.shuffle(cards);
        //Distribution des cartes
        int numjoueur=0;
        while(!cards.isEmpty()){
            Player currentPlayer=players.get(numjoueur);
            Card currentCard=cards.get(0);
            currentPlayer.addComponent(currentCard);
            cards.remove(currentCard);
            numjoueur++;
            if(numjoueur>=players.size())
                numjoueur=0;
        }
        System.out.println("Game started: "+name);
        displayState();
        Scanner scanner= new Scanner(System.in);
        System.out.println("Press Enter to continue");
        scanner.nextLine();
        //Rounds:
        while(!end()){
            numberOfRounds++;
            System.out.println("----Round "+numberOfRounds+"----");
            //Tous les 10 rounds, chaque joueurs mélange sa main:
            if (numberOfRounds%10 == 0) {
                System.out.println("Every players shuffle their cards !");
                for(Player player: players){
                    if(player.isPlaying())
                        player.shuffleHand();
                }
            }
            //Chaque joueur joue sa 1ere carte, on conserve la plus haute valeur jouée:
            Map<Player,Card> playedCard= new HashMap<>();
            int highestValue=0;
            for(Player player : players){
                if(player.isPlaying()) {
                    /*
                    Card card = (Card) player.play();
                    card.setHidden(false);
                    playedCard.put(player, card);
                    System.out.println(player.getName() + " has played " + card.getName());
                    if (card.getValue() > highestValue)
                        highestValue = card.getValue();
                     */
                }
            }
            //On determine la liste de gagnants:
            List<Player> winners=new ArrayList<>();
            for(Map.Entry<Player,Card> entry: playedCard.entrySet()){
                Card card= entry.getValue();
                if(card.getValue()==highestValue)
                    winners.add(entry.getKey());
            }
            /*
            Changements pour l'égalité
             */
            Player winner= null;
            //Liste des cartes à remettre, qui sera agrandie en cas d'égalité
            List<Card> winnerCards= new ArrayList<>();
            for(Map.Entry<Player,Card> entry: playedCard.entrySet()){
                Card card=entry.getValue();
                winnerCards.add(card);
            }
            //Si il y a plusieurs personne dans la liste winners, il y a égalité:
            while(winners.size()>1) {
                System.out.println("Egalité !");
                //On ajoute une carte face cachée
                for (Player player : winners) {
                    if(player.getScore()!=0){
                        /*
                        Card card= (Card) player.play();
                        winnerCards.add(card);

                         */
                    }
                    //Si le joueur n'a plus de cartes, un adversaire en joue une aléatoirement pour lui
                    else{
                        System.out.print(player.getName() + " has no more cards, he takes a card of ");
                        List<Player> winnerGiveCard=new ArrayList<>(winners);
                        Collections.shuffle(winnerGiveCard); //On shuffle pas directement winners puisque on est en train de la parcourir au dessus
                        boolean played= false;
                        for(Player opponent: winnerGiveCard){
                            if(opponent.getScore()!=0 && !played){
                                /*
                                Card card=(Card) opponent.play();
                                winnerCards.add(card);
                                System.out.println(opponent.getName());
                                played=true;

                                 */
                            }
                            //Si aucun des gagnants n'a de cartes, on en prends au joueur qui a le plus de cartes
                            if(!played){
                                Player mostCards=players.get(0);
                                for(Player player1: players){
                                    if(player1.getScore()>mostCards.getScore())
                                        mostCards=player1;
                                }
                                /*
                                Card card=(Card)mostCards.play();
                                winnerCards.add(card);
                                System.out.println(mostCards.getName());

                                 */
                            }
                        }
                    }
                    System.out.println(player.getName() + " added a hidden card");
                }
                //On lance un round avec une nouvelle carte:
                Map<Player,Card> playedCard2= new HashMap<>();
                int highestValue2=0;
                for (Player player : winners) {
                    Card card=null;
                    if(player.getScore()!=0){
                        /*
                        card= (Card) player.play();
                        card.setHidden(false);
                        playedCard2.put(player,card);
                         */
                    }
                    //Si le joueur n'a plus de cartes, un adversaire en joue une aléatoirement pour lui
                    else{
                        System.out.print(player.getName() + " has no more cards, he takes a card of ");
                        List<Player> winnerGiveCard=new ArrayList<>(winners);;
                        Collections.shuffle(winnerGiveCard);
                        boolean played= false;
                        for(Player opponent: winnerGiveCard){
                            if(opponent.getScore()!=0 && !played){
                                /*
                                card=(Card) opponent.play();
                                card.setHidden(false);
                                playedCard2.put(player,card);
                                System.out.println(opponent.getName());
                                played=true;
                                 */
                            }
                        }
                        //Si aucun des gagnants n'a de cartes, on en prends au joueur qui a le plus de cartes
                        if(!played) {
                            Player mostCards = players.get(0);
                            for (Player player1 : players) {
                                if (player1.getScore() > mostCards.getScore())
                                    mostCards = player1;
                            }
                            /*
                            card = (Card) mostCards.play();
                            card.setHidden(false);
                            playedCard2.put(player, card);
                            System.out.println(mostCards.getName());
                             */
                        }
                    }
                    if (card.getValue() > highestValue2)
                        highestValue2 = card.getValue();
                    System.out.println(player.getName() + " has played " + card.getName());
                }
                //Le nouveau gagnant emporte tout:
                winners.clear();
                for(Map.Entry<Player,Card> entry: playedCard2.entrySet()){
                    Card card= entry.getValue();
                    if(card.getValue()==highestValue2)
                        winners.add(entry.getKey());
                    //On ajoute aussi cette pile à celle des autres cartes à donner au vainqueur:
                    winnerCards.add(entry.getValue());
                }
                System.out.println("End of the battle !\nWinners:");
                for(Player player: winners){
                    System.out.println(player);
                }
                //Pour vérifier les égalités:
                /*
                System.out.println("Press Enter to continue");
                scanner.nextLine();
                */
            }
            //Le gagnant est le 1er élément de winners
            winner=winners.get(0);
            System.out.print("=>Round Winner is "+winner.getName()+". He goes from "+winner.getScore()+" to ");
            
            //Le gagnant récupère toutes les cartes:
            for(Card card: winnerCards){
                card.setHidden(true);
                winner.addComponent(card);
            }
            System.out.println(winner.getScore()+" cards !");

            //On change l'état de ceux qui ne peuvent plus jouer:
            for(Player player: players){
                if(player.getScore()==0 && player.isPlaying()){
                    player.canPlay(false);
                    displayState();
                    System.out.println(player.getName()+" lost, he is out of Cards !");
                    /*
                    System.out.println("Press Enter to continue");
                    scanner.nextLine();
                     */
                }
            }
        }
        //A la fin des rounds, on renvoie le gagnant, cad le seul qui peut encore jouer:
        for(Player player: players){
            if(player.isPlaying()) {
                player.canPlay(false);
                return player;
            }
        }
        //En cas d'erreur
        return null;
    }
}
