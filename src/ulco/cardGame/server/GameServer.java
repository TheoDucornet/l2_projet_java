package ulco.cardGame.server;

import ulco.cardGame.common.games.BattleGame;
import ulco.cardGame.common.games.CardGame;
import ulco.cardGame.common.games.PokerGame;
import ulco.cardGame.common.players.CardPlayer;
import ulco.cardGame.common.interfaces.Game;
import ulco.cardGame.common.interfaces.Player;
import ulco.cardGame.common.players.PokerPlayer;

/**
 * Server Game management with use of Singleton instance creation
 */
public class GameServer {

    public static void main(String[] args) {
        /*
        //Number of players to test with
        int numberPlayers= 3;

        //System.out.println("Here the created games will be launched!");
        // TODO
        // - Create Game Instance
        //Game game= new CardGame("CardGame",numberPlayers,"resources/games/cardGame.txt");
        Game game=new PokerGame("PokerGame",numberPlayers,"resources/games/pokerGame.txt");
        // - Add players to game
        for(int i=0;i<numberPlayers;i++){
            String name="user "+(i+1);
            Player player= new PokerPlayer(name);
            game.addPlayer(player);
        }
        game.displayState();
        // - Run the Game loop
        Player winner = game.run();
        game.displayState();
        // - Display the winner player
        System.out.println("==>Winner is "+winner.getName()+" ("+winner.getScore()+"points) ! Congratulations !");
         */
    }
}

